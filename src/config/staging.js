export default {
  ui: {
    baseUrl: 'https://taymor-ui-staging.herokuapp.com'
  },
  server: {
    baseUrl: 'https://taymor-services-staging.herokuapp.com'
  },
  logger: {
    level: 'error'
  }
}
