export default {
  ui: {
    baseUrl: 'http://localhost:3000'
  },
  server: {
    baseUrl: 'http://localhost:5000'
  },
  logger: {
    level: 'info'
  }
}
