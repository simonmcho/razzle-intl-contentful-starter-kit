export default {
  ui: {
    baseUrl: 'http://localhost:3000',
    contentful: {
      spaceId: process.env.CONTENTFUL_SPACE_ID,
      deliveryToken: process.env.CONTENTFUL_DELIVERY_TOKEN
    }
  },
  server: {
    baseUrl: 'http://localhost:5000'
  },
  logger: {
    level: 'info'
  }
}
