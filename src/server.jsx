/* eslint-disable indent */
/* eslint-disable import/no-dynamic-require */

import React from 'react'
import express from 'express'
import cookieParser from 'cookie-parser'
import { renderToString } from 'react-dom/server'
import { Provider } from 'react-redux'
import { StaticRouter } from 'react-router-dom'
import { toJSON } from 'transit-immutable-js'

import configureStore from './configureStore'
import Routes from './ui/routes'

import { determineUserLang } from './utils/intl'
import { getIntlContent } from './utils/contentful'

import { NAMESPACE as localization } from './ui/data/localization/constants'

const assets = require(process.env.RAZZLE_ASSETS_MANIFEST)

const server = express()
server
  .disable('x-powered-by')
  .use(express.static(process.env.RAZZLE_PUBLIC_DIR))
  .use(cookieParser())
  // eslint-disable-next-line consistent-return
  .get('/*', async (req, res) => {
    const context = {}
    const lang = determineUserLang(req.acceptsLanguages(), req.path)
    const data = await getIntlContent(req.path)
    const preloadedState = {
      [localization]: data
    }

    const store = configureStore(preloadedState)
    const markup = renderToString(
      <Provider store={store}>
        <StaticRouter context={context} location={req.url}>
          <Routes lang={lang} />
        </StaticRouter>
      </Provider>
    )

    const styleTags = ''
    // const styleTags = sheet.getStyleTags()

    /**
     * Hard to get immutable json from ssr to browser due to
     *  @see https://github.com/reduxjs/redux/issues/1555#issuecomment-220101348
     *
     * Using transit-js to help with this
     */
    const finalState = toJSON(store.getState())

    if (context.url) {
      res.redirect(context.url)
    } else {
      // const html = render(markup)
      // res.status(200).send(html)
      res.status(200).send(
        `<!doctype html>
          <html lang="">
          <head>
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta charset="utf-8" />
            <meta name="google-site-verification" content="LcKYrO2mXIoXqrtF0ZscSt225o58xFMKg28V6X0oWDg" />
            ${
              // Prevent indexing for now
              process.env.NODE_ENV === 'production' ? '<meta name="robots" content="noindex">' : ''
            }
            <title>Welcome to Razzle</title>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            ${
              assets.client.css
                ? `<link rel="stylesheet" href="${assets.client.css}">`
                : ''
            }
            ${styleTags}
            ${
              process.env.NODE_ENV === 'production'
                ? `<script src="${assets.client.js}" defer></script>`
                : `<script src="${assets.client.js}" defer crossorigin></script>`
            }
            <script>
              window.__PRELOADED_STATE__ = '${finalState}';
            </script>
          </head>
          <body>
              <div id="root">${markup}</div>
          </body>
        </html>`
      )
    }
  })

export default server
