import React from 'react'
import logo from './react.svg'
import './Home.css'

import { FormattedMessage } from 'react-intl'

import Sample from './component'

class Home extends React.Component {
  render() {
    return (
      <div>
        <h1>
          Hello world
        </h1>
        <FormattedMessage id="home.title" />
        <FormattedMessage id="home.subtitle" />
      </div>
    )
  }
}

export default Home
