import React, { useEffect, useState } from 'react'
// import { useDispatch, useSelector } from 'react-redux'
// import { useHistory } from 'react-router-dom'
import pt from 'prop-types'

const Document = ({ children }) => {
  const [bodyStyles, setBodyStyles] = useState('')

  // if (!windowResized) return <div> </div>
  return (
    <div>
      {children}
      <style jsx="true" global="true">
        {`
          html, body, div, span, applet, object, iframe,
          h1, h2, h3, h4, h5, h6, p, blockquote, pre,
          a, abbr, acronym, address, big, cite, code,
          del, dfn, em, img, ins, kbd, q, s, samp,
          small, strike, strong, sub, sup, tt, var,
          b, u, i, center,
          dl, dt, dd, ol, ul, li,
          fieldset, form, label, legend,
          table, caption, tbody, tfoot, thead, tr, th, td,
          article, aside, canvas, details, embed, 
          figure, figcaption, footer, header, hgroup, 
          menu, nav, output, ruby, section, summary,
          time, mark, audio, video {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            font: inherit;
            vertical-align: baseline;
            font-family: sans-serif;
          }
          /* HTML5 display-role reset for older browsers */
          article, aside, details, figcaption, figure, 
          footer, header, hgroup, menu, nav, section {
            display: block;
          }
          * {
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
          }
          body {
            line-height: 1;
            ${bodyStyles}
          }
          ol, ul {
            list-style-type: none;
          }
          h1 {
            font-size: 36px;
            font-weight: bold;
          }
          h2 {
            font-size: 28px;
            font-weight: bold;
          }
          h3 {
            font-size: 20px;
            font-weight: bold;
          }
          h4 {
            font-size: 14px;
            font-weight: bold;
          }
          img {
            width: 100%;
            height: auto;
          }
          button {
            padding: 0;
            background-color: transparent;
            border: none;
            cursor: pointer;
          }
          input {
            border-radius: 0;
            -webkit-border-radius: 0;
          }
        `}
      </style>
    </div>
  )
}

Document.propTypes = {
  children: pt.node.isRequired
}

export default Document
