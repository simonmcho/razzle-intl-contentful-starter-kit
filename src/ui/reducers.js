import { combineReducers } from 'redux'

import { NAMESPACE as localization } from './data/localization/constants'
import localizationReducer from './data/localization/reducer'

const rootReducer = combineReducers({
  [localization]: localizationReducer
})

export default rootReducer
