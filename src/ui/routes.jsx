import React from 'react'
import pt from 'prop-types'
import { useSelector } from 'react-redux'
import { Route, Switch } from 'react-router-dom'
import { IntlProvider } from 'react-intl'

import logger from '../logger'

import { getLocalization } from './data/localization/selectors'

import Document from './_document'
import Home from './pages/Home'

const Routes = ({ lang }) => {
  const messages = useSelector(getLocalization)
  return (
    <IntlProvider
      onError={(err) => logger.error(err)}
      locale={lang}
      messages={messages[lang]}
      defaultLocale="en"
    >
      <Document>
        <Switch>
          <Route exact path="/" component={Home} />
        </Switch>
      </Document>
    </IntlProvider>
  )
}

Routes.propTypes = {
  lang: pt.string.isRequired
}

export default Routes
