import * as constants from './constants'

// eslint-disable-next-line import/prefer-default-export
export const getLocalization = (state) => state[constants.NAMESPACE]
