import { fromJS } from 'immutable'

import * as keyNames from './keyNames'

export const defaultState = fromJS({
  [keyNames.EN]: {},
  [keyNames.FR]: {}
})

export default (state = defaultState, action) => {
  switch (action.type) {
    default:
      return state
  }
}
