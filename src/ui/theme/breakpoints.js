/* eslint-disable prefer-destructuring */

const breakpoints = [
  '30em', // 480px
  '40em', // 640px
  '52em', // 832px
  '64em', // 1024px
  '80em', // 1280px
  '91.5em', // 1464px
  '100em' // 1600px
]

export const socksBreakpoints = [
  { xs: 0 },
  { s: 480 },
  { m: 640 },
  { xm: 832 },
  { l: 1024 },
  { xl: 1280 },
  { xxl: 1464 },
  { xxxl: 1600 }
]

export const pxBreakpoints = {
  xs: 0,
  s: 480,
  m: 640,
  xm: 832,
  l: 1024,
  xl: 1280,
  xxl: 1464,
  xxxl: 1600
}

// aliases
breakpoints.xs = 0
breakpoints.s = breakpoints[0]
breakpoints.m = breakpoints[1]
breakpoints.xm = breakpoints[2]
breakpoints.l = breakpoints[3]
breakpoints.xl = breakpoints[4]
breakpoints.xxl = breakpoints[5]
breakpoints.xxxl = breakpoints[6]

export default breakpoints
