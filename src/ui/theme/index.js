import breakpoints from './breakpoints'
import space from './space'
import colors from './colors'

export default {
  breakpoints,
  space,
  colors
}
