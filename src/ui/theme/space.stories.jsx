/* eslint-disable import/no-extraneous-dependencies */

import React from 'react'
import { Box } from '@rebass/grid'
import { withKnobs, number } from '@storybook/addon-knobs'

export default {
  title: 'Layout/Space',
  decorators: [withKnobs]
}

const options = {
  range: true,
  min: 0,
  max: 18,
  step: 1
}

export const DefaultCarousel = () => {
  return (
    <Box
      pb={number('Space level', 1, options)}
      style={{ backgroundColor: 'grey' }}
    />

  )
}
