import React from 'react'
import pt from 'prop-types'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'

const ProviderWrapper = ({ children, store }) => (
  <Provider store={store}>
    <BrowserRouter>
      {children}
    </BrowserRouter>
  </Provider>
)

ProviderWrapper.propTypes = {
  children: pt.node.isRequired,
  store: pt.object.isRequired
}

export default ProviderWrapper
