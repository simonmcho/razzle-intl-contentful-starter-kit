import React from 'react'
import { hydrate } from 'react-dom'
import { fromJSON } from 'transit-immutable-js'

import configureStore from './configureStore'

import Provider from './provider'
import Routes from './ui/routes'

import { determineUserLang } from './utils/intl'

const lang = determineUserLang(
  navigator.languages || [],
  window.location.pathname
)

/**
 * Hard to get immutable json from ssr to browser due to
 *  @see https://github.com/reduxjs/redux/issues/1555#issuecomment-220101348
 *
 * Using transit-js to help with this
 */
const preloadedState = window.__PRELOADED_STATE__
  ? fromJSON(window.__PRELOADED_STATE__)
  : undefined

// Allow the passed state to be garbage-collected
delete window.__PRELOADED_STATE__

const store = configureStore(preloadedState)

hydrate(
  <Provider store={store}>
    <Routes lang={lang} />
  </Provider>,
  document.getElementById('root'),
  () => {
    // [ReHydratation](https://github.com/cssinjs/jss/blob/master/docs/ssr.md)
    const jssStyles = document.getElementById('jss-ssr')
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles)
    }
  }
)

if (module.hot) {
  module.hot.accept()
}
