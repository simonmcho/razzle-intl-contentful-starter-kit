/**
 * Wrappper of the logger (either console or any other transport we use)
 *
 * See: https://github.com/pinojs/pino
 * For more transports: https://github.com/pinojs/pino/blob/master/docs/transports.md
 *
 * Note: any function that has access to the fastify { request, reply } objects
 *  should be using the request.log.info('hello world') syntax for logs
 */

const pino = require('pino')

const env = typeof window !== 'undefined' ? 'production' : process.env.NODE_ENV

module.exports = pino({
  browser: { asObject: true },
  prettyPrint: env !== 'production',
  level: env !== 'production' ? 'trace' : 'info'
})
