import { createClient } from 'contentful'
import config from '../config'

const contentParser = (data) => {
  const structure = {
    en: {},
    fr: {}
  }
  if (!data.items) throw new Error(`Expected items key in "data" param, received ${data}`)

  const { items } = data
  const parsed = items.reduce((accum, item) => {
    const { textContent } = item.fields
    const formatted = { ...accum }
    Object.keys(textContent).forEach((key) => {
      formatted[key] = {
        ...formatted[key],
        ...textContent[key]
      }
    })
    return formatted
  }, structure)
  return parsed
}

const getClient = async () => {
  const { spaceId, deliveryToken } = config.ui.contentful
  const client = await createClient({
    space: spaceId,
    accessToken: deliveryToken
  })
  return client
}

const pathnameParser = (pathname) => pathname === '/' ? 'home' : pathname

// eslint-disable-next-line import/prefer-default-export
export const getIntlContent = async (pathname) => {
  const client = await getClient()
  const data = await client.getEntries({
    content_type: 'internationalization',
    include: '4',
    'fields.title': pathnameParser(pathname)
  })

  return contentParser(data)
}
