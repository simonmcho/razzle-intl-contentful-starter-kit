export const defaultLang = 'en'
export const supportedLangs = {
  en: 'English',
  fr: 'French'
}

const stripCountry = (lang) => (
  lang
    .trim()
    .replace('_', '-')
    .split('-')[0]
)

const findFirstSupported = (languages) => {
  const supported = Object.keys(supportedLangs)
  return languages.find((code) => supported.includes(code))
}

export function determineUserLang(acceptedLangs, path = null) {
  if (path !== null) {
    const urlLang = path.trim().split('/')[1]

    const matchingUrlLang = findFirstSupported([stripCountry(urlLang)])

    if (matchingUrlLang) {
      return matchingUrlLang
    }
  }

  const acceptedLangCodes = acceptedLangs.map(stripCountry)
  const supportedLangCodes = Object.keys(supportedLangs)
  const matchingLangCode = acceptedLangCodes
    .find((code) => supportedLangCodes.includes(code)) // need polyfill

  return matchingLangCode || defaultLang
}
