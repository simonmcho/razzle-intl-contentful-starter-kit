# Razzle starter kit using react-intl and contentful for dynamic localization content
Start by using the razzle starter kit

```
npx create-razzle-app my-app
```

This uses `<IntlProvider />` component to wrap the `<Routes>`. The props passed into this component determines what content to use based on the language setting from the user's browser:
```
<IntlProvider
  onError={(err) => logger.error(err)}
  locale={lang}
  messages={messages[lang]}
  defaultLocale="en"
>
  // child components here
</IntlProvider>
```

The `messages` object can be a static object that holds all the localized content in code, and therefore in memory. This starter kit takes one step further, using contentful data to populate the messages object before rendering to string from the server side. Then the client side takes over.

To improve on this, we should create a caching layer to prevent the same contentful api call for the same localized content.

### 1. Set the directory up
```
.gitignore
package.json
razzle.config.js
public
src
  | client.jsx (client.js)
  | configure-store.js
  | index.js 
  | logger.js
  | server.js
  | ui
    | _document.jsx (app.js)
    | routes.jsx
    | components
    | pages
      | Home
        | index.js
```
*Nameing rules*
1. All react components are CamalCase and starts with Capitial
2. All else are snake-case and all lower case

### 2. Adding other good stuff
```
npx install-peerdeps --dev eslint-config-airbnb
```

Setup eslintrc
```
{
  "extends": "airbnb",
  "plugins": [
    "jest",
    "import"
  ],
  "env": {
    "jest/globals": true,
    "browser": true,
    "node": true
  },
  "rules": {
    "arrow-body-style": "off",
    "class-methods-use-this": "off",
    "comma-dangle": ["error", "never"],
    "global-require": "off",
    "import/default": "warn",
    "import/named": "error",
    "import/prefer-default-export": "warn",
    "react/jsx-props-no-spreading": "off",
    "no-alert": "error",
    "no-bitwise": "off",
    "no-console": "error",
    "no-underscore-dangle": "off",
    "padded-blocks": "error",
    "semi": ["error", "never"],
    "quotes": ["error", "single"]
  }
}
```

setup jest
Add setup-jest folder in root (file mock-request-animation-frame.js)
```
global.requestAnimationFrame = function(callback) {
  setTimeout(callback, 0)
}
```


```
const path = require('path')

module.exports = {
  rootDir: './src',
  collectCoverage: true,
  coverageDirectory: '<rootDir>/../coverage',
  coverageThreshold: {
    global: {
      branches: 0,
      functions: 0,
      lines: 0,
      statements: 0
    }
  },
  collectCoverageFrom: [
    '**/*.{js,jsx}'
  ],
  transform: {
    '^.+\\.jsx?$': 'babel-jest'
  },
  setupFiles: [
    path.resolve('setup-jest/mock-request-animation-frame.js')
  ],
  coveragePathIgnorePatterns: [
    '/node_modules/',
    '/coverage/',
    'logger.js'
  ],
  testMatch: ['**/__tests__/**/*.js?(x)', '**/?(*.)(spec|test).js?(x)']
}
```

setup npm scripts
```
    "lint:deps": "npx updated",
    "lint:js": "npx eslint src",
    "lint:ec": "npx editorconfig-checker -exclude '(node_modules|\\.git|build|public).*' .",
    "lint": "npx npm-run-all -p lint:*",
    "test": "npm run lint && npm run unit && npm audit --audit-level=moderate",
    "unit": "jest",
```


### 3. Add styled-components
Using [styled-components](https://www.styled-components.com/) and [styled-system](https://github.com/styled-system/styled-system)
```
npm i styled-components styled-system
```

With styled-components, need to do some magic to make it work well with SSR
https://www.styled-components.com/docs/advanced#server-side-rendering

Need to also modify client.jsx hydrate method
```
() => {
    // [ReHydratation](https://github.com/cssinjs/jss/blob/master/docs/ssr.md)
    const jssStyles = document.getElementById('jss-ssr')
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles)
    }
  }
```

### 4. Add redux, thunk
